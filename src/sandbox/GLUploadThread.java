/*
 * Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package sandbox;

import java.nio.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

import android.util.Log;
import android.graphics.SurfaceTexture;
import android.opengl.GLUtils;
import android.view.Surface;

import java.lang.Thread;
import java.util.concurrent.Semaphore;
import java.util.concurrent.locks.LockSupport;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ArrayBlockingQueue;

import junit.framework.Assert;

import javax.microedition.khronos.egl.EGL10;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.egl.EGLContext;
import javax.microedition.khronos.egl.EGLDisplay;
import javax.microedition.khronos.egl.EGLSurface;
import javax.microedition.khronos.opengles.GL;

import static android.opengl.GLES20.*;

public class GLUploadThread extends GLThread {

    static final String TAG = "GLUploadThread";

    public FrameStats mFrameStats;
 
    // For uploading to a single EglImage.
    int mTexture;
    int mEglImage;
    int mR, mG, mB, mA;

    // For uploading to EglImages.
    int[] mTextures;
    int[] mEglImages;
 
    // For uploading to surfaces.
    Surface[] mSurfaces;
    BlockingQueue<Surface> mProducedSurfaces;
    BlockingQueue<Surface> mConsumedSurfaces;

    int mUploadCount;

    private IntBuffer[] mPixelBuffers;
    private int mNextPixelBuffer;
    private Random mRandom = new Random();
    private int mTileWidth = -1;
    private int mTileHeight = -1;
    private boolean initialized = false;
    private Semaphore mWorkReadySemaphore;
    private Semaphore mNeedsWorkSemaphore;

    GLUploadThread(int tileWidth, int tileHeight) {
        mTileWidth = tileWidth;
        mTileHeight = tileHeight;

        // Allocate buffers for some raw pixel data.
        int[] pixelData = new int[mTileWidth * mTileHeight];
        mPixelBuffers = new IntBuffer[7];
        for (int i = 0; i < mPixelBuffers.length; i++) {
            mPixelBuffers[i] = ByteBuffer.allocateDirect(pixelData.length * 4)
                    .order(ByteOrder.nativeOrder()).asIntBuffer();
            Arrays.fill(pixelData, mRandom.nextInt());
            mPixelBuffers[i].put(pixelData).position(0);
        }

        mWorkReadySemaphore = new Semaphore(0);
        mNeedsWorkSemaphore = new Semaphore(1);
    }

    void release(Semaphore semaphore) {
        semaphore.release();
    }

    void acquire(Semaphore semaphore) {
        try {
            semaphore.acquire();
        } catch(java.lang.InterruptedException e) {
            Log.e(TAG, "Interurrupted acquiring semaphore");
        }
    }

    public void uploadToEglImages(int[] eglImages, int times) {
        acquire(mNeedsWorkSemaphore);
        Assert.assertEquals(mEglImages, null);
        Assert.assertEquals(mTextures, null);
        mEglImages = eglImages;
        mUploadCount = times;
        release(mWorkReadySemaphore);
    }

    public void uploadToEglImage(int eglImage, int r, int g, int b, int a) {
        acquire(mNeedsWorkSemaphore);
        mEglImage = eglImage;
        mR = r;
        mG = g;
        mB = b;
        mA = a;
        release(mWorkReadySemaphore);
    }

    public void uploadToSurfaces(Surface[] surfaces, int times) {
        acquire(mNeedsWorkSemaphore);
        mUploadCount = times;
        if (surfaces == mSurfaces) {
            release(mWorkReadySemaphore);
            return;
        }
        Assert.assertEquals(mSurfaces, null);
        Assert.assertEquals(mTextures, null);
        mSurfaces = surfaces;
        mProducedSurfaces = new ArrayBlockingQueue<Surface>(mSurfaces.length);
        mConsumedSurfaces = new ArrayBlockingQueue<Surface>(mSurfaces.length);
        for (int i = 0; i < mSurfaces.length; i++) {
            try { mConsumedSurfaces.put(mSurfaces[i]); }
            catch (InterruptedException e) { throw new RuntimeException(); }
        }
        release(mWorkReadySemaphore);
    }

    public boolean doneUploading() {
        return mNeedsWorkSemaphore.availablePermits() == 1;
    }

    public void waitForDoneUploading() {
        acquire(mNeedsWorkSemaphore);
        release(mNeedsWorkSemaphore);
    }

    public void init() {
        if (initialized)
            return;
        initialized = true;
        initGL();
        GLCHK();
    }

    private void DoSurfaceUploads() {
        try { 
            mFrameStats = new FrameStats();
            for(int i = 0; i < mUploadCount; i++) {
                mFrameStats.startFrame();
                Surface surface = mConsumedSurfaces.take();
                GLUtil.uploadToSurface(surface, mTileWidth, mTileHeight);
                //GLUtil.uploadToSurface(mSurfaces[i % mSurfaces.length], mTileWidth, mTileHeight);
                mProducedSurfaces.put(surface);
                mFrameStats.endFrame();
            }
        } catch (InterruptedException e) {
            throw new RuntimeException("Failed DoSurfaceUploads!");
        }
        mFrameStats.logStats("Threaded Surface Upload Stats");
    }

    ArrayList<Surface> TakeProducedSurfaces() {
        ArrayList<Surface> surfaces = new ArrayList<Surface>();
        Surface surface;
        while (surfaces.size() < 10 && (surface = mProducedSurfaces.poll()) != null)
            surfaces.add(surface);
        return surfaces;
    }

    void PutConsumedSurfaces(ArrayList<Surface> surfaces) {
        try { 
            for(int i = 0; i < surfaces.size(); i++)
                mConsumedSurfaces.put(surfaces.get(i));
        } catch (InterruptedException e) {
            throw new RuntimeException("Failed PutConsumedSurfacs");
        }
    }

    public int mSync;
    private void DoEglImageUpload() {
        if (mTexture == 0) {
            int[] temp = new int[1];
            glGenTextures(1, temp, 0);
            mTexture = temp[0];
            GLUtil.initTextureFromEglImage(mTexture, mEglImage); GLCHK();
        }
        GLUtil.uploadWithTexSubImage(mTexture, 1, 1, mR, mG, mB, mA); GLCHK();
        mSync = GLUtil.createSync();
    }

    private void DoEglImageUploads() {
        Assert.assertEquals(mTextures, null); GLCHK();
        Assert.assertNotNull(mEglImages);

        // Generate textures, and assign the EGL images to the textures.
        mTextures = new int[mEglImages.length];
        glGenTextures(mTextures.length, mTextures, 0);
        for (int i = 0; i < mTextures.length; i++) {
            GLUtil.initTextureFromEglImage(mTextures[i], mEglImages[i]); GLCHK();
        }

        mFrameStats = new FrameStats();
        for(int i = 0; i < mUploadCount; i++) {
            mFrameStats.startFrame();
            GLUtil.uploadWithTexSubImage(mTextures[i % mTextures.length], mTileWidth, mTileWidth,
                                         mPixelBuffers[GLUtil.randomIndex(mPixelBuffers.length)]); GLCHK();
            GLUtil.waitSync(GLUtil.createSync());
            mFrameStats.endFrame();
        }
        mFrameStats.logStats("Threaded EGLImage Upload Stats");

        glDeleteTextures(mTextures.length, mTextures, 0);
        mTextures = null;
        mEglImages = null;

        Assert.assertEquals(EGL10.EGL_SUCCESS, mEgl.eglGetError());
        Assert.assertEquals(GL_NO_ERROR, glGetError());
    }

    @Override
    public void run() {
        init();

        int uploadRounds = 2; // Can allocate in between to detect memory leaks.
        for (int i = 0; i < uploadRounds; i++) {
            acquire(mWorkReadySemaphore);
            if (mEglImage != 0)
              DoEglImageUpload();
            if (mEglImages != null)
              DoEglImageUploads();
            if (mSurfaces != null)
              DoSurfaceUploads();
            release(mNeedsWorkSemaphore); 
        }

        destroyGL(); 
    }
}
