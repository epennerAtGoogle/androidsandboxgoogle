/*
 * Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package sandbox;

import android.animation.ObjectAnimator;
import android.animation.AnimatorSet;
import android.app.Activity;
import android.graphics.SurfaceTexture;
import android.view.Surface;
import android.os.Bundle;
import android.util.Log;
import android.view.TextureView;
import android.view.SurfaceView;
import android.view.Surface;
import android.view.SurfaceHolder;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import junit.framework.Assert;

public class SandboxActivity extends Activity implements SurfaceHolder.Callback {

    static final String TAG = "SandboxActivity";

    public static GLFrameRenderer mRenderer;
    public static boolean mAnimateView = false;

    private SurfaceView mSurfaceView;
    private TextureView mTextureView;
    private final Semaphore mSemaphore = new Semaphore(0);
    public GLProducerThread mProducerThread;
    public FrameStats mFrameStats;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        System.loadLibrary("producer");

        int frames = 1000;
        int layers = 1;
        int tileWidth = 256;
        int tileHeight = 256;
        int texturesToUpload = 1;
        boolean recycleTextures = true;
        GLTextureUploadRenderer.UploadType uploadType = GLTextureUploadRenderer.UploadType.SurfaceTexture;

        mRenderer = new GLTextureUploadRenderer(frames,
                                                layers,
                                                tileWidth,
                                                tileHeight,
                                                texturesToUpload,
                                                recycleTextures,
                                                uploadType);

        mSurfaceView = new SurfaceView(this);
        mSurfaceView.getHolder().addCallback(this);
        setContentView(mSurfaceView);
    }

    // SurfaceView callbacks.

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        Log.w(TAG, "surfaceChanged");
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        Log.w(TAG, "surfaceCreated");

        mProducerThread = new GLProducerThread(mRenderer, holder, mSemaphore);
        mProducerThread.start();
        mFrameStats = new FrameStats();
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        Log.w(TAG, "surfaceDestroyed");
    }


    // TextureView callbacks.
/* 

    @Override
    public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
        mProducerThread = new GLProducerThread(mRenderer, surface, mSemaphore);
        mProducerThread.start();
        mFrameStats = new FrameStats();
    }

    @Override
    public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {
    }

    @Override
    public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
        mProducerThread = null;
        return true;
    }

    @Override
    public void onSurfaceTextureUpdated(SurfaceTexture surface) {
        mFrameStats.endFrame();
        mFrameStats.startFrame();
    }
*/

}
