/*
 * Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package sandbox;

import static android.opengl.GLES20.*;
import javax.microedition.khronos.egl.EGL10;
import javax.microedition.khronos.egl.EGLContext;
import javax.microedition.khronos.egl.EGLSurface;
import javax.microedition.khronos.egl.EGLDisplay;

import junit.framework.Assert;
import java.nio.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.locks.LockSupport;
import android.graphics.SurfaceTexture;
import android.opengl.GLUtils;
import android.util.Log;
import android.view.Surface;

public class GLTextureUploadRenderer implements GLFrameRenderer{

    public enum UploadType {EglImage, SurfaceTexture};

    static final String TAG = "GLTextureUploadRenderer";
    static final int GL_TEXTURE_EXTERNAL_OES = 0x8D65;

    private int mScrollOffset;

    private int mTileWidth;
    private int mTileHeight;

    private int mScreenWidth;
    private int mScreenHeight;
    private int mTileCountX;
    private int mTileCountY;

    private int mFramesRendered;
    private int mFramesToRender;
    private int mLayers;
    private int mUploadsPerFrame;
    private boolean mRecycleTextures;
    private UploadType mUploadType;

    private int mTextureCount = 0;
    private int[] mTextures;
    private int[] mFbos;
    private int[] mEglImages;
    private Surface[] mSurfaces;
    private SurfaceTexture[] mSurfaceTextures;
    private int[] mSurfaceTextureTextures;

    private IntBuffer[] mPixelBuffers;
    private int mNextPixelBuffer;

    private FloatBuffer mQuadVertBuffer;
    private Program mProgram2D;
    private Program mProgramExternal;

    public GLUploadThread mUploadThread;

    private FrameStats mAttachStats = new FrameStats();
    private FrameStats mAttachBatchStats = new FrameStats();
    private FrameStats mDetachStats = new FrameStats();
    private FrameStats mDetachBatchStats = new FrameStats();
    private FrameStats mUpdateStats = new FrameStats();
    private FrameStats mUpdateBatchStats = new FrameStats();
    private FrameStats mReleaseStats = new FrameStats();
    private FrameStats mReleaseBatchStats = new FrameStats();
    private FrameStats mBlitStats = new FrameStats();
    private FrameStats mBlitBatchStats = new FrameStats();
    private FrameStats mDrawStats = new FrameStats();
    private FrameStats mDrawBatchStats = new FrameStats();

    private FrameStats mFrameStats = new FrameStats();

    GLTextureUploadRenderer(int frames,
                            int layers,
                            int tileWidth,
                            int tileHeight,
                            int texturesToUpload,
                            boolean recycleTextures,
                            UploadType uploadType) {
        mFramesToRender = frames;
        mLayers = layers;
        mTileWidth = tileWidth;
        mTileHeight = tileHeight;
        mUploadsPerFrame = texturesToUpload;
        mRecycleTextures = recycleTextures;
        mUploadType = uploadType;
    }

    @Override
    public void init(int width, int height) {

        GLUtil.setCommonState();

        // 25MB of textures for testing.
        int budgetBytes = 1024 * 1024 * 25;
        int textureBytes = mTileWidth * mTileHeight * 4;
        mTextureCount = budgetBytes / textureBytes;

        Log.e(TAG, "TextureCount " + mTextureCount);

        // Worst case, tiles straddling viewport
        mScreenWidth = width;
        mScreenHeight = height;
        mTileCountX = (mScreenWidth + mTileWidth)  / mTileWidth + 1;
        mTileCountY = (mScreenHeight + mTileHeight) / mTileHeight + 1;

        initPixelBuffers();

        mQuadVertBuffer = GLUtil.createUnitQuadVertBuffer();

        // Create shader programs
        mProgram2D = new Program(mVertexShader, mFragmentShader2D, GL_TEXTURE_2D);
        mProgramExternal = new Program(mVertexShader, mFragmentShaderExternal, GL_TEXTURE_EXTERNAL_OES);

        GLCHK();

        mUploadThread = new GLUploadThread(mTileWidth, mTileHeight);
        mUploadThread.start();
    }

    void initTextures(boolean uploadData) {
        if (mTextures != null)
            return;
        mTextures = new int[mTextureCount];
        glGenTextures(mTextureCount, mTextures, 0);
        for (int i = 0; i < mTextureCount; i++) {
            GLUtil.initTexture(mTextures[i], mTileWidth, mTileWidth);
            if (uploadData)
                GLUtil.uploadWithTexImage(mTextures[i], mTileWidth, mTileWidth,
                                          mPixelBuffers[GLUtil.randomIndex(mPixelBuffers.length)]);
        }
    }

    void initFbos() {
        if (mFbos != null)
            return;
        mFbos = new int[mTextureCount];
        glGenFramebuffers(mTextureCount, mFbos, 0);
        for (int i = 0; i < mTextureCount; i++) {
            glBindFramebuffer(GL_FRAMEBUFFER, mFbos[i]);
            glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,  GL_TEXTURE_2D, mTextures[i], 0);
        }
        glBindFramebuffer(GL_FRAMEBUFFER, 0);
    }

    void uploadToTextures() {
        for (int i = 0; i < mTextureCount; i++) {
            GLUtil.uploadWithTexImage(mTextures[i], mTileWidth, mTileWidth,
                                      mPixelBuffers[GLUtil.randomIndex(mPixelBuffers.length)]);
        }
    }

    void destroyTextures() {
        if (mTextures == null)
            return;
        glDeleteTextures(mTextures.length, mTextures, 0);
        mTextures = null;
    }

    void initEglImages() {
        if (mEglImages != null)
            return;
        mEglImages = new int[mTextureCount];
        for (int i = 0; i < mTextureCount; i++) {
            mEglImages[i] = GLUtil.createEglImageFromTexture(mTextures[i]);
        }
    }

    void destroyEglImages() {
        if (mEglImages == null)
            return;
        for (int i = 0; i < mTextureCount; i++)
            GLUtil.destroyEglImage(mEglImages[i]);
        mEglImages = null;
    }

    void initSurfaceTextures() {
        if (mSurfaceTextures != null)
            return;
        mSurfaceTextureTextures = new int[mTextureCount];
        glGenTextures(mTextureCount, mSurfaceTextureTextures, 0);
        mSurfaceTextures = new SurfaceTexture[mTextureCount];
        mSurfaces = new Surface[mTextureCount];
        for (int i = 0; i < mTextureCount; i++) {
            boolean singleBuffer = true;
            mSurfaceTextures[i] = new SurfaceTexture(mSurfaceTextureTextures[i], singleBuffer);
            mSurfaceTextures[i].setDefaultBufferSize(1, 1);
            mSurfaces[i] = new Surface(mSurfaceTextures[i]);           
            GLUtil.uploadToSurface(mSurfaces[i], mTileWidth, mTileHeight);
            mSurfaceTextures[i].updateTexImage();

            // Detach the texture from GL. This deletes the texture so
            // we set it to zero and glGenTextures when we attach again.           
            //mSurfaceTextures[i].detachFromGLContext();
            //mTextures[i] = 0;

            Log.e(TAG, "Created Surface Texture: " + i);
        }
    }

    void destroySurfaceTextures() {
        if (mSurfaceTextures == null)
            return;
        for (int i = 0; i < mTextureCount; i++) {
            mSurfaceTextures[i].release();
            mSurfaces[i].release();
        }
        mSurfaceTextures = null;
        mSurfaces = null;
    }

    void initPixelBuffers() {
        // Allocate buffers for some raw pixel data.
        mPixelBuffers = new IntBuffer[10];
        for (int i = 0; i < mPixelBuffers.length; i++)
            mPixelBuffers[i] = GLUtil.createFilledPixelBuffer(mTileWidth, mTileWidth);
    }

    @Override
    public void shutdown() {
        try {
            mUploadThread.join();
        } catch (InterruptedException e) {
        }
        Assert.assertTrue(glGetError() ==  GL_NO_ERROR);
    }

    private void drawQuad(Program program, int texture, int screenWidth, int screenHeight, int offsetX, int offsetY) {
        glUseProgram(program.mProgram);

        glVertexAttribPointer(program.mCoordHandle, 2, GL_FLOAT, false, 8, mQuadVertBuffer);
        glEnableVertexAttribArray(program.mCoordHandle);
        glUniform1i(program.mTextureHandle, 0);
        glUniform2f(program.mScreenSizeHandle, screenWidth, screenHeight);
        glUniform2f(program.mTileSizeHandle, mTileWidth, mTileHeight);
        glUniform2f(program.mOffsetHandle, offsetX, offsetY);
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(program.mTextureTarget, texture);
        glDrawArrays(GL_TRIANGLES, 0, 6);
    }

    public void LogBatchStats(String name,  FrameStats stats, FrameStats batchStats) {
       if (batchStats.mFrameCount == 0)
           return;
       stats.logStats(name + " Stats");
       batchStats.logStats(name + " Batch Stats (for "
                                + ((float)stats.mFrameCount / (float)batchStats.mFrameCount)
                                + " textures)");
    }

    @Override
    public boolean isFinished() {
        return !mUploadThread.isAlive();
    }

    private int textureIndex(int tileX, int tileY, int tileZ) {
        return (tileX + tileY * mTileCountX + tileZ * mTileCountX * mTileCountY) % mTextures.length;
    }

    private void drawTile(int i, int j, int k, int textureIndex) {
        mDrawStats.startFrame();
        Program program = mProgram2D;
        int texture = mTextures[textureIndex];
        //Program program = (mUploadType == UploadType.SurfaceTexture) ? mProgramExternal : mProgram2D;
        //int texture = (mUploadType == UploadType.SurfaceTexture)
        //              ? mSurfaceTextureTextures[textureIndex]
        //              : mTextures[textureIndex];
        if (k > 0)
            glEnable(GL_BLEND);
        else
            glDisable(GL_BLEND);
        drawQuad(program, texture, mScreenWidth, 
                                   mScreenHeight, i * mTileWidth - mTileWidth / 2 + k * 8,
                                                  j * mTileHeight - mScrollOffset + k * 8);
                                                  // j * mTileHeight - 0 + k * 8);
        mDrawStats.endFrame();
    }

    void attachTile(int textureIndex) {
        // The texture was deleted on the last detach, so glGenTextures a new one and attach to it.
        if (mSurfaceTextureTextures[textureIndex] != 0)
            return;
        mAttachStats.startFrame();
        glGenTextures(1, mSurfaceTextureTextures, textureIndex);
        mSurfaceTextures[textureIndex].attachToGLContext(mSurfaceTextureTextures[textureIndex]);
        mAttachStats.endFrame();
    }

    void detachTile(int textureIndex) {
        // The texture is deleted during detach, so we assign it to 0.
        if (mSurfaceTextureTextures[textureIndex] == 0)
            return;
        mDetachStats.startFrame();
        mSurfaceTextureTextures[textureIndex] = 0;
        mSurfaceTextures[textureIndex].detachFromGLContext();
        mDetachStats.endFrame();
    }

    void releaseTile(int textureIndex) {
        if (mSurfaceTextureTextures[textureIndex] == 0)
            return;
        mReleaseStats.startFrame();
        mSurfaceTextures[textureIndex].releaseTexImage();
        mReleaseStats.endFrame();
    }

    void updateTile(int textureIndex) {
        if (mSurfaceTextureTextures[textureIndex] == 0)
            return;
        mUpdateStats.startFrame();
        mSurfaceTextures[textureIndex].updateTexImage();
        mUpdateStats.endFrame();
    }

    private void blitTile(int textureIndex) {
        // Blit from the SurfaceTexture to regular texture.
        mBlitStats.startFrame();
        glBindFramebuffer(GL_FRAMEBUFFER, mFbos[textureIndex]);
        drawQuad(mProgramExternal, mSurfaceTextureTextures[textureIndex], mTileWidth, mTileHeight, 0, 0);
        
        // Test blitting from 2D to 2D texture.
        // glBindFramebuffer(GL_FRAMEBUFFER, mFbos[textureIndex]);
        // drawQuad(mProgram2D, mTextures[(textureIndex+1)%mTextures.length], mTileWidth, mTileHeight, 0, 0);
        
        // Test using glBlitFrameBuffer from 2D to 2D.
        // glBindFramebuffer(GL_DRAW_FRAMEBUFFER, mFbos[textureIndex]);
        // glBindFramebuffer(GL_READ_FRAMEBUFFER, mFbos[(textureIndex+1)%mTextures.length]);
        // glBlitFramebuffer(0, 0, mTileWidth, mTileHeight, 0, 0, mTileWidth, mTileHeight, GL_COLOR_BUFFER_BIT, GL_NEAREST);

        mBlitStats.endFrame();
    }

    interface TileAction {
        void doTile(int i, int j, int k, int textureIndex);
    }

    private TileAction mDrawTile = new TileAction() {
        public void doTile(int i, int j, int k, int textureIndex) { drawTile(i, j, k, textureIndex); }
    };

    private TileAction mAttachTile = new TileAction() {
        public void doTile(int i, int j, int k, int textureIndex) { attachTile(textureIndex); }
    };

    private TileAction mDetachTile = new TileAction() {
        public void doTile(int i, int j, int k, int textureIndex) { detachTile(textureIndex); }
    };

    private TileAction mReleaseTile = new TileAction() {
        public void doTile(int i, int j, int k, int textureIndex) { releaseTile(textureIndex); }
    };

    private TileAction mUpdateTile = new TileAction() {
        public void doTile(int i, int j, int k, int textureIndex) { updateTile(textureIndex); }
    };

    private TileAction mBlitTile = new TileAction() {
        public void doTile(int i, int j, int k, int textureIndex) { blitTile(textureIndex); }
    };

    private TileAction mAttachAndDrawTile = new TileAction() {
        public void doTile(int i, int j, int k, int textureIndex) {
            mAttachTile.doTile(i, j, k, textureIndex);
            mDrawTile.doTile(i, j, k, textureIndex);
        }
    };

    private void iterateOverFrameTiles(TileAction tileAction) {
        int tileStartY = mScrollOffset / mTileHeight;
        for(int k = 0; k < mLayers; k++) {
            for(int i = 0; i < mTileCountX; i++) {
                for(int j = tileStartY; j < tileStartY + mTileCountY; j++) {
                    tileAction.doTile(i, j, k, textureIndex(i, j, k));
                }
            }
        }
    }

    private void iterateOverTileRange(int startIndex, int count, TileAction tileAction) {
        for(int i = 0; i < count; i++) {
            int index = (startIndex + i) % mTextures.length;
            tileAction.doTile(0, 0, 0, index);
        }
    }

    private void drawTilesForFrame() {
        mDrawBatchStats.startFrame();
        iterateOverFrameTiles(mDrawTile);
        mDrawBatchStats.endFrame();
    }

    private void attachTilesForFrame() {
        mAttachBatchStats.startFrame();
        iterateOverFrameTiles(mAttachTile);
        mAttachBatchStats.endFrame();
    }

    private void detachTilesForFrame() {
        mDetachBatchStats.startFrame();
        iterateOverFrameTiles(mDetachTile);
        mDetachBatchStats.endFrame();
    }

    private void releaseTilesForFrame() {
        mReleaseBatchStats.startFrame();
        iterateOverFrameTiles(mReleaseTile);
        mReleaseBatchStats.endFrame();
    }

    private void updateTilesForFrame() {
        mUpdateBatchStats.startFrame();
        iterateOverFrameTiles(mUpdateTile);
        mUpdateBatchStats.endFrame();
    }


    private void attachTilesInRange(int index, int count) {
        mAttachBatchStats.startFrame();
        iterateOverTileRange(index, count, mAttachTile);
        mAttachBatchStats.endFrame();
    }

    private void detachTilesInRange(int index, int count) {
        mDetachBatchStats.startFrame();
        iterateOverTileRange(index, count, mDetachTile);
        mDetachBatchStats.endFrame();
    }

    private void releaseTilesInRange(int index, int count) {
        mReleaseBatchStats.startFrame();
        iterateOverTileRange(index, count, mReleaseTile);
        mReleaseBatchStats.endFrame();
    }

    private void updateTilesInRange(int index, int count) {
        mUpdateBatchStats.startFrame();
        iterateOverTileRange(index, count, mUpdateTile);
        mUpdateBatchStats.endFrame();
    }

    private void blitTilesInRange(int index, int count) {
        mBlitBatchStats.startFrame();
        iterateOverTileRange(index, count, mBlitTile);
        mBlitBatchStats.endFrame();
    }

    private static int mCurrentSurfaceToConsume = 0;
    private void BlitProducedSurfacesToTextures() {
        ArrayList<Surface> surfaces = mUploadThread.TakeProducedSurfaces();

        glViewport(0, 0, mTileWidth, mTileHeight);
        
        updateTilesInRange(mCurrentSurfaceToConsume, surfaces.size());
        blitTilesInRange(mCurrentSurfaceToConsume, surfaces.size());
        releaseTilesInRange(mCurrentSurfaceToConsume, surfaces.size());

        glBindFramebuffer(GL_FRAMEBUFFER, 0);
        glViewport(0, 0, mScreenWidth, mScreenHeight);

        mCurrentSurfaceToConsume += surfaces.size();
        mCurrentSurfaceToConsume %= mSurfaces.length;
        mUploadThread.PutConsumedSurfaces(surfaces);
    }

    public void LogAndResetStats() {
        mFrameStats.logStats("Full Frame Stats");
        LogBatchStats("Draw", mDrawStats, mDrawBatchStats);
        LogBatchStats("Release", mReleaseStats, mReleaseBatchStats);
        LogBatchStats("Update", mUpdateStats, mUpdateBatchStats);
        LogBatchStats("Blit", mBlitStats, mBlitBatchStats);

        mFrameStats = new FrameStats();
        mDrawStats = new FrameStats();
        mDrawBatchStats = new FrameStats();
        mReleaseStats = new FrameStats();
        mReleaseBatchStats = new FrameStats();
        mUpdateStats = new FrameStats();
        mUpdateBatchStats = new FrameStats();
        mBlitStats = new FrameStats();
        mBlitBatchStats = new FrameStats();
    }

    public void renderFrame() {
        mFrameStats.endFrame();
        mFrameStats.startFrame();

        if (mFramesRendered % 1000 == 0)
            LogAndResetStats();

        if (mUploadType == UploadType.EglImage) {
            initTextures(true);
            initEglImages();
        }
        if (mUploadType == UploadType.SurfaceTexture) {
            initTextures(true);
            initFbos();
            initSurfaceTextures();
        }

        if (mUploadThread.doneUploading()) {
            if (mUploadType == UploadType.EglImage) {
                mUploadThread.uploadToEglImages(mEglImages, 10000);
            } else if (mUploadType == UploadType.SurfaceTexture) {
                // Make sure the surfaces are 'consumed' to start.
                mUploadThread.uploadToSurfaces(mSurfaces, 10000);
            } else {
                throw new RuntimeException("Unsupported upload type.");
            }
        }

        if (mUploadType == UploadType.SurfaceTexture) {
            BlitProducedSurfacesToTextures();
        }

        //attachTilesForFrame();
        //GLUtil.waitSync(mUploadThread.mSync);
        drawTilesForFrame();
        //detachTilesForFrame();

        mScrollOffset += 16;
        mFramesRendered++;
    }

    void GLCHK() {
        int error;
        while ((error = glGetError()) != GL_NO_ERROR) {
            Log.e(TAG, "glError " + error);
            throw new RuntimeException("glError " + error);
        }
    }

    private final String mVertexShader =
        "uniform vec2 screenSize;\n" +
        "uniform vec2 tileSize;\n" +
        "uniform vec2 offset;\n" +
        "attribute vec2 coord;\n" +
        "varying vec2 vTexCoord;\n" +
        "void main() {\n" +
        "  vec2 screenCoord = coord * tileSize + offset; \n" +
        "  vec2 ndcCoord = screenCoord / screenSize * 2.0 - 1.0; \n" +
        "  gl_Position = vec4(ndcCoord, 0.0, 1.0);\n" +
        "  vTexCoord = coord;\n" +
        "}\n";

    private final String mFragmentShader2D =
        "precision mediump float;\n" +
        "varying vec2 vTexCoord;\n" +
        "uniform sampler2D texture;\n" +
        "uniform vec4 test; \n" +
        "void main() {\n" +
        "  gl_FragColor = texture2D(texture, vTexCoord); \n" +
        "}\n";

    private final String mFragmentShaderExternal =
        "#extension GL_OES_EGL_image_external : require \n" +
        "precision mediump float;\n" +
        "varying vec2 vTexCoord;\n" +
        "uniform samplerExternalOES texture;\n" +
        "void main() {\n" +
        "  gl_FragColor = texture2D(texture, vTexCoord); \n" +
        "}\n";

    private int loadShader(int shaderType, String source) {
        int shader = glCreateShader(shaderType);
        glShaderSource(shader, source);
        glCompileShader(shader);
        int[] compiled = new int[1];
        glGetShaderiv(shader, GL_COMPILE_STATUS, compiled, 0);
        Assert.assertTrue(compiled[0] == GL_TRUE);
        return shader;
    }

    private int createProgram(String vertexSource, String fragmentSource) {
        int vertexShader = loadShader(GL_VERTEX_SHADER, vertexSource);
        int pixelShader = loadShader(GL_FRAGMENT_SHADER, fragmentSource);
        int program = glCreateProgram();
        glAttachShader(program, vertexShader);
        glAttachShader(program, pixelShader);
        glLinkProgram(program);
        int[] linkStatus = new int[1];
        glGetProgramiv(program, GL_LINK_STATUS, linkStatus, 0);
        Assert.assertTrue(linkStatus[0] == GL_TRUE);
        return program;
    }

    private class Program {
        public Program(String vertexShader, String fragmentShader, int target) {
            mProgram = createProgram(vertexShader, fragmentShader);
            mScreenSizeHandle = glGetUniformLocation(mProgram, "screenSize");
            mTileSizeHandle = glGetUniformLocation(mProgram, "tileSize");
            mOffsetHandle = glGetUniformLocation(mProgram, "offset");
            mTextureHandle = glGetUniformLocation(mProgram, "texture");
            mCoordHandle = glGetAttribLocation(mProgram, "coord");
            mTextureTarget = target;
        }
        public int mProgram;
        public int mScreenSizeHandle;
        public int mTileSizeHandle;
        public int mOffsetHandle;
        public int mCoordHandle;
        public int mTextureHandle;
        public int mTextureTarget;
    }
}
