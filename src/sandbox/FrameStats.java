/*
 * Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package sandbox;

import android.util.Log;


public class FrameStats {
    public long mUsedFrameCount;
    public long mTotalFrameTime;
    public long mFrameMax;
    public long mFrameMin = Long.MAX_VALUE;
    public long mFrameAve;
    public float mFrameMaxMs;
    public float mFrameMinMs;
    public float mFrameAveMs;
    public long mFramesAbovep1ms;
    public long mFramesAbovep2ms;
    public long mFramesAbovep5ms;
    public long mFramesAbove01ms;
    public long mFramesAbove02ms;
    public long mFramesAbove04ms;
    public long mFramesAbove06ms;
    public long mFramesAbove10ms;
    public long mFramesAbove15ms;
    public long mFramesAbove17ms;
    public long mFramesAbove20ms;
    public long mFramesAbove25ms;
    public long mFramesAbove30ms;

    public long mFrameCount;
    public long mFrameStartTime;

    public final float NS_TO_MS = (1.0f / 1000000.0f);
    public final float MS_TO_NS = 1000000;

    public void startFrame() {
        mFrameStartTime = GLUtil.getCpuTime();
        //mFrameStartTime = GLUtil.getClockTime();
    }

    public long endFrame() {
        if(mFrameStartTime == 0)
            return 0;
        long time = GLUtil.getCpuTime();
        //long time = GLUtil.getClockTime();
 
        long frameTime = time - mFrameStartTime;
        // Ignore the first few frames to improve stats.
        if (mFrameCount >= 10) {
            mUsedFrameCount++;
            mTotalFrameTime += frameTime;
            mFrameAve = mUsedFrameCount > 0 ? mTotalFrameTime / mUsedFrameCount : 0;
            mFrameMax = (frameTime > mFrameMax) ? frameTime : mFrameMax;
            mFrameMin = (frameTime < mFrameMin) ? frameTime : mFrameMin;
            mFrameAveMs = (float)mFrameAve * NS_TO_MS;
            mFrameMinMs = (float)mFrameMin * NS_TO_MS;
            mFrameMaxMs = (float)mFrameMax * NS_TO_MS;

            mFramesAbovep1ms += (frameTime > 0.1 * MS_TO_NS) ? 1 : 0;
            mFramesAbovep2ms += (frameTime > 0.2 * MS_TO_NS) ? 1 : 0;
            mFramesAbovep5ms += (frameTime > 0.5 * MS_TO_NS) ? 1 : 0;
            mFramesAbove01ms += (frameTime > 01 * MS_TO_NS) ? 1 : 0;
            mFramesAbove02ms += (frameTime > 02 * MS_TO_NS) ? 1 : 0;
            mFramesAbove04ms += (frameTime > 04 * MS_TO_NS) ? 1 : 0;
            mFramesAbove06ms += (frameTime > 06 * MS_TO_NS) ? 1 : 0;
            //mFramesAbove10ms += (frameTime > 10 * MS_TO_NS) ? 1 : 0;
            //mFramesAbove15ms += (frameTime > 15 * MS_TO_NS) ? 1 : 0;
            //mFramesAbove17ms += (frameTime > 17 * MS_TO_NS) ? 1 : 0;
            //mFramesAbove20ms += (frameTime > 20 * MS_TO_NS) ? 1 : 0;
            //mFramesAbove25ms += (frameTime > 25 * MS_TO_NS) ? 1 : 0;
            //mFramesAbove30ms += (frameTime > 30 * MS_TO_NS) ? 1 : 0;
        }
        mFrameCount++;
        mFrameStartTime = 0;
        return frameTime;
    }

    public void logStats(String tag) {
        Log.w("Stats", "----------- " + tag + " -------------");
        Log.w("Stats", "Frame stats for " + mUsedFrameCount + " frames");
        Log.w("Stats", "Min (ms): " + mFrameMinMs);
        Log.w("Stats", "Max (ms): " + mFrameMaxMs);
        Log.w("Stats", "Ave (ms): " + mFrameAveMs);
        Log.w("Stats", "Over .1 ms: " + mFramesAbovep1ms + "   " + (((float)mFramesAbovep1ms / (float)mUsedFrameCount) * 100.0f) + "%");
        Log.w("Stats", "Over .2 ms: " + mFramesAbovep2ms + "   " + (((float)mFramesAbovep2ms / (float)mUsedFrameCount) * 100.0f) + "%");
        Log.w("Stats", "Over .5 ms: " + mFramesAbovep5ms + "   " + (((float)mFramesAbovep5ms / (float)mUsedFrameCount) * 100.0f) + "%");
        Log.w("Stats", "Over 01 ms: " + mFramesAbove01ms + "   " + (((float)mFramesAbove01ms / (float)mUsedFrameCount) * 100.0f) + "%");
        Log.w("Stats", "Over 02 ms: " + mFramesAbove02ms + "   " + (((float)mFramesAbove02ms / (float)mUsedFrameCount) * 100.0f) + "%");
        Log.w("Stats", "Over 04 ms: " + mFramesAbove04ms + "   " + (((float)mFramesAbove04ms / (float)mUsedFrameCount) * 100.0f) + "%");
        Log.w("Stats", "Over 06 ms: " + mFramesAbove06ms + "   " + (((float)mFramesAbove06ms / (float)mUsedFrameCount) * 100.0f) + "%");
        //Log.w("Stats", "Over 10 ms: " + mFramesAbove10ms + "   " + (((float)mFramesAbove10ms / (float)mUsedFrameCount) * 100.0f) + "%");
        //Log.w("Stats", "Over 15 ms: " + mFramesAbove15ms + "   " + (((float)mFramesAbove15ms / (float)mUsedFrameCount) * 100.0f) + "%");
        //Log.w("Stats", "Over 17 ms: " + mFramesAbove17ms + "   " + (((float)mFramesAbove17ms / (float)mUsedFrameCount) * 100.0f) + "%");
        //Log.w("Stats", "Over 20 ms: " + mFramesAbove20ms + "   " + (((float)mFramesAbove20ms / (float)mUsedFrameCount) * 100.0f) + "%");
        //Log.w("Stats", "Over 25 ms: " + mFramesAbove25ms + "   " + (((float)mFramesAbove25ms / (float)mUsedFrameCount) * 100.0f) + "%");
        //Log.w("Stats", "Over 30 ms: " + mFramesAbove30ms + "   " + (((float)mFramesAbove30ms / (float)mUsedFrameCount) * 100.0f) + "%");
    }
}



