/*
 * Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <cutils/log.h>
#include <jni.h>
#include <JNIHelp.h>

#include <android/native_window.h>
#include <android/native_window_jni.h>
#include <EGL/egl.h>
#include <EGL/eglext.h>
#include <GLES2/gl2.h>
#include <GLES2/gl2ext.h>
#include <GLES3/gl3.h>
#include <GLES3/gl3ext.h>

#include <stdlib.h>
#include <time.h>

#define LOGE(...) __android_log_print(ANDROID_LOG_ERROR, LOG_TAG, __VA_ARGS__)

#define GLCHK() check_gl(__LINE__);
void check_gl(int line) {
  int error;
  while ((error = glGetError()) != GL_NO_ERROR) {
    LOGE("GL ERROR %i on line %i", error, line);
  }
}

// EGLImage
PFNEGLCREATEIMAGEKHRPROC eglCreateImageKHR_EXT = NULL;
PFNEGLDESTROYIMAGEKHRPROC eglDestroyImageKHR_EXT = NULL;
PFNGLEGLIMAGETARGETTEXTURE2DOESPROC glEGLImageTargetTexture2DOES_EXT = NULL;

// EGLFenceSync
PFNEGLCREATESYNCKHRPROC eglCreateSyncKHR_EXT = NULL;
PFNEGLDESTROYSYNCKHRPROC eglDestroySyncKHR_EXT = NULL;
PFNEGLWAITSYNCKHRPROC eglWaitSyncKHR_EXT = NULL;
PFNEGLCLIENTWAITSYNCKHRPROC eglClientWaitSyncKHR_EXT = NULL;
PFNEGLSIGNALSYNCKHRPROC eglSignalSyncKHR_EXT = NULL;

jboolean lazyInitExtensions() {
  if (!glEGLImageTargetTexture2DOES_EXT ||
      !eglCreateImageKHR_EXT ||
      !eglDestroyImageKHR_EXT) {
    glEGLImageTargetTexture2DOES_EXT = (PFNGLEGLIMAGETARGETTEXTURE2DOESPROC) eglGetProcAddress("glEGLImageTargetTexture2DOES");
    eglCreateImageKHR_EXT = (PFNEGLCREATEIMAGEKHRPROC) eglGetProcAddress("eglCreateImageKHR");
    eglDestroyImageKHR_EXT = (PFNEGLDESTROYIMAGEKHRPROC) eglGetProcAddress("eglDestroyImageKHR");
    if (!glEGLImageTargetTexture2DOES_EXT ||
        !eglCreateImageKHR_EXT ||
        !eglDestroyImageKHR_EXT) {
      LOGE("Failed to EGLImageKHR functions!");
      return false;
    }
  }
  if (!eglCreateSyncKHR_EXT ||
      !eglDestroySyncKHR_EXT ||
      !eglWaitSyncKHR_EXT ||
      !eglClientWaitSyncKHR_EXT ||
      !eglSignalSyncKHR_EXT) {
    eglCreateSyncKHR_EXT = (PFNEGLCREATESYNCKHRPROC) eglGetProcAddress("eglCreateSyncKHR");
    eglDestroySyncKHR_EXT = (PFNEGLDESTROYSYNCKHRPROC) eglGetProcAddress("eglDestroySyncKHR");
    eglWaitSyncKHR_EXT = (PFNEGLWAITSYNCKHRPROC) eglGetProcAddress("eglWaitSyncKHR");
    eglClientWaitSyncKHR_EXT = (PFNEGLCLIENTWAITSYNCKHRPROC) eglGetProcAddress("eglClientWaitSyncKHR");
    eglSignalSyncKHR_EXT = (PFNEGLSIGNALSYNCKHRPROC) eglGetProcAddress("eglSignalSyncKHR");
    if (!eglCreateSyncKHR_EXT ||
        !eglDestroySyncKHR_EXT ||
        !eglWaitSyncKHR_EXT ||
        !eglSignalSyncKHR_EXT) {
      LOGE("Failed to EGLFenceSync functions!");
      return false;
    }
  }

  return true;
}

static jint createEglImageFromTexture(JNIEnv*, jclass, int textureId) {
  if (!lazyInitExtensions() || !textureId)
    return 0;
  EGLDisplay display = eglGetCurrentDisplay();
  EGLContext context = eglGetCurrentContext();
  EGLenum target = EGL_GL_TEXTURE_2D_KHR;
  EGLClientBuffer buffer = (EGLClientBuffer) textureId;
  EGLint attrib_list[] = {
      EGL_GL_TEXTURE_LEVEL_KHR, 0,         // mip map level to reference.
      EGL_IMAGE_PRESERVED_KHR, EGL_TRUE,   // preserve the data in the texture.
      EGL_NONE
  };
  int eglImageAsInt =  (int)eglCreateImageKHR_EXT(display, context, target, buffer, attrib_list);
  //LOGE("Created EGLImageKHR! %i", eglImageAsInt);
  return eglImageAsInt;
}

static jint bindEglImageToOpenglTexture2d(JNIEnv*, jclass, int eglImage)
{
  if (!lazyInitExtensions() || !eglImage)
    return 0;
  glEGLImageTargetTexture2DOES_EXT(GL_TEXTURE_2D, (EGLImageKHR)eglImage);
  return 1;
}

static jint destroyEglImage(JNIEnv*, jclass, int eglImage)
{
  if (!lazyInitExtensions() || !eglImage)
    return 0;
  EGLDisplay display = eglGetCurrentDisplay();
  eglDestroyImageKHR_EXT(display, (EGLImageKHR)eglImage);
  //LOGE("Destroyed EGLImageKHR! %i", eglImage);
  return 1;
}

static jint uploadWithTexSubImage(JNIEnv *env, jclass clazz, jint texture, jint width, jint height, jint r, jint g, jint b, jint a) {
  unsigned int* pixels = new unsigned int[width * height];
  unsigned int value = (a << 24) + (b << 16) + (g << 8) + r;
  for (int i = 0, n = width * height; i < n; i++)
    pixels[i] = value;

  glActiveTexture(GL_TEXTURE0);
  glBindTexture(GL_TEXTURE_2D, texture);
  glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, width, height, GL_RGBA, GL_UNSIGNED_BYTE, pixels);

  delete[] pixels;
  return true;
}

static jint uploadToSurface(JNIEnv *env, jclass clazz, jobject jsurface, jint width, jint height, jint r, jint g, jint b, jint a) {
  ANativeWindow* window = ANativeWindow_fromSurface(env, jsurface);
  if (!window) {
    LOGE("No window!");    
    return false;
  }

  ANativeWindow_Buffer buffer;
  ARect rect = {0, 0, width, height};

  ANativeWindow_setBuffersGeometry(window, width, height, WINDOW_FORMAT_RGBA_8888);
  ANativeWindow_lock(window, &buffer, NULL);
  int* pixels = reinterpret_cast<int*>(buffer.bits);

  // TODO: Should we check the return value of lock/unlockAndPost?
  if (buffer.stride * buffer.height < width * height) {
    LOGE("Failed to lock buffer.");
    return false;
  }

  unsigned int value = (a << 24) + (b << 16) + (g << 8) + r;
  for (int i = 0, n = buffer.stride * buffer.height; i < n; i++)
    pixels[i] = value;

  ANativeWindow_unlockAndPost(window);
  ANativeWindow_release(window);
  return true;
}

static jint createSync(JNIEnv *env, jclass clazz) {
  EGLSyncKHR sync = eglCreateSyncKHR_EXT(eglGetCurrentDisplay(),
                                         EGL_SYNC_FENCE_KHR, // EGL_SYNC_REUSABLE_KHR,
                                         NULL);
  if (sync == EGL_NO_SYNC_KHR)
    LOGE("Failed in eglCreateSyncKHR!");

  //if (!eglSignalSyncKHR_EXT(eglGetCurrentDisplay(), sync, EGL_UNSIGNALED_KHR))
  //  LOGE("Failed in eglSignalSyncKHR!");

  //if (!eglClientWaitSyncKHR_EXT(eglGetCurrentDisplay(), sync, EGL_SYNC_FLUSH_COMMANDS_BIT_KHR, EGL_FOREVER_KHR))
  //  LOGE("Failed in eglWaitSyncKHR!");
  //if (!eglDestroySyncKHR_EXT(eglGetCurrentDisplay(), sync))
  //  LOGE("Failed in eglDestroySyncKHR!");

  return (int) sync;
}

static jint waitSync(JNIEnv *env, jclass clazz, int intSync) {
  EGLSyncKHR sync = (EGLSyncKHR)intSync;
  //if (!eglWaitSyncKHR_EXT(eglGetCurrentDisplay(), sync, 0))
  //  LOGE("Failed in eglWaitSyncKHR!");
  if (!eglClientWaitSyncKHR_EXT(eglGetCurrentDisplay(), sync, EGL_SYNC_FLUSH_COMMANDS_BIT_KHR, EGL_FOREVER_KHR))
    LOGE("Failed in eglWaitSyncKHR!");
  return true;
}

static jint destroySync(JNIEnv *env, jclass clazz, int intSync) {
  EGLSyncKHR sync = (EGLSyncKHR)intSync;
  if (!eglDestroySyncKHR_EXT(eglGetCurrentDisplay(), sync))
    LOGE("Failed in eglDestroySyncKHR!");
  return true;
}

static jlong getCpuTime(JNIEnv *env, jclass clazz) {
  timespec time;
  clock_gettime(CLOCK_THREAD_CPUTIME_ID, &time);
  jlong nanos = time.tv_nsec;
  nanos += ((jlong)time.tv_sec) * 1000000000;
  return nanos;
}

static jint createPbo(JNIEnv *env, jclass clazz) {
  static const int PBO_SIZE = 5 * 1024 * 1024;
  GLuint buffer = 0;
  glGenBuffers(1, &buffer);
  glBindBuffer(GL_PIXEL_UNPACK_BUFFER, buffer);
  glBufferData(GL_PIXEL_UNPACK_BUFFER, PBO_SIZE, 0, GL_STREAM_DRAW);
  GLubyte* ptr = (GLubyte*)glMapBufferRange(GL_PIXEL_UNPACK_BUFFER, 0, PBO_SIZE, GL_MAP_WRITE_BIT);
  glBindBuffer(GL_PIXEL_UNPACK_BUFFER, 0);
  LOGE("ESP Created PBO %i", buffer);
  LOGE("ESP Created PBO at address %p", ptr);
  GLCHK();
  return buffer;
}

static JNINativeMethod methods[] = {
  // name, signature, function
  { "nativeGetCpuTime", "()J", (void *) getCpuTime },
  { "nativeCreateEglImageFromTexture", "(I)I", (void *) createEglImageFromTexture },
  { "nativeBindEglImageToOpenglTexture2d", "(I)I", (void *) bindEglImageToOpenglTexture2d },
  { "nativeDestroyEglImage", "(I)I", (void *) destroyEglImage },
  { "nativeuploadWithTexSubImage", "(IIIIIII)I", (void *) uploadWithTexSubImage },
  { "nativeUploadToSurface", "(Landroid/view/Surface;IIIIII)I", (void *) uploadToSurface },
  { "nativeCreateSync", "()I", (void *) createSync },
  { "nativeWaitSync", "(I)I", (void *) waitSync },
  { "nativeDestroySync", "(I)I", (void *) destroySync },
  { "nativeCreatePbo", "()I", (void *) createPbo },
};

jint JNI_OnLoad(JavaVM *vm, void *reserved) {
  JNIEnv *env = NULL;

  if (vm->GetEnv((void **) &env, JNI_VERSION_1_4) != JNI_OK) {
    return JNI_ERR;
  }

  if (jniRegisterNativeMethods(env,
                               "sandbox/GLUtil",
                               methods, sizeof(methods) / sizeof(JNINativeMethod))) {
    LOGE("Failed to register JNI methods.");
    return JNI_ERR;
  }

  return JNI_VERSION_1_4;
}

